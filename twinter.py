# Python 3.x

import twint

# Configure
c = twint.Config()
c.Username = "txtdrprogramer" # twitter username
c.Store_json = True # json format file
c.Output = "txtdrprogramer" # output file

# Run
twint.run.Search(c)

# Issue:
# 1. Cannot crawl more than 20 tweets with default twint package

# Challenge:
# 1. Store to another format:
#     - CSV
#     - SQL DB (MySQL, PosgreSQL, etc)
#     - NoSQL DB (ELK, MongoDB, etc)