from instagram_private_api import Client, ClientCompatPatch
import json

user_name = '<instagram_username>'
password = '<instagram_password>'

# Komik Harian
api = Client(user_name, password)
result = api.username_info('komikharian_') # Instagram username
# result = api.user_feed('4432539410') # User profile id -> get from api.username_info()
# result = result.get('items', []) # only get the content

with open('instagrem_result' + '.json', 'w') as outfile:
    json.dump(result, outfile, indent=4)

# Challenge:
# 1. How to get all content from user feed
# 2. Store to another format:
#     - CSV
#     - SQL DB (MySQL, PosgreSQL, etc)
#     - NoSQL DB (ELK, MongoDB, etc)